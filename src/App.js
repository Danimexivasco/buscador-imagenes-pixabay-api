import React, { useState, useEffect } from 'react';
import Formulario from './components/Formulario'
import ListadoImagenes from './components/ListadoImagenes'

function App() {

  const [termino, guardarTermino] = useState('');
  const [imagenes, guardarImagenes] = useState([])
  const [paginaactual, guardarPaginaActual] = useState(1);
  const [paginastotales, guardarPaginasTotales] = useState(1);
  const [numpaginas, guardarNumPaginas] = useState([])




  useEffect(() => {
    const llamadaAPI = async () => {
      if (termino.trim() === '') return;
      const resultadosPagina = 30;
      const key = '15568195-0d76373529da990ecbc718f5c';
      const url = `https://pixabay.com/api/?key=${key}&q=${termino}&per_page=${resultadosPagina}&page=${paginaactual}`

      const respuesta = await fetch(url);
      const resultado = await respuesta.json();
      // console.log(resultado.hits)
      guardarImagenes(resultado.hits)
      const calcularPaginasTotales = Math.ceil(resultado.totalHits / resultadosPagina)
      guardarPaginasTotales(calcularPaginasTotales);

      // Mostrar paginas 
      const calcularNumeroPaginas = () => {
        // console.log(paginastotales)
        const numerospaginas = []
        for (let i = 1; i <= paginastotales; i++) {
          numerospaginas.push(i)
        }
        // console.log(numerospaginas)
        guardarNumPaginas(numerospaginas);
      }
      calcularNumeroPaginas();

      // document.getElementsByClassName('page-item').classList.remove('active')
      eliminarActivo()
      document.querySelector(`[id='${paginaactual}']`).classList.add('active')
      // const lead = document.querySelector('.lead');
      // lead.scrollIntoView({ behavior: 'smooth' });
      window.scrollTo({ top: 0, behavior: 'smooth' });
    }
    llamadaAPI();
  }, [termino, paginaactual, paginastotales])



  const eliminarActivo = () => {
    var links = document.querySelectorAll('.page-item')
    for (let i = 0; i < links.length; i++) {
      links[i].classList.remove('active')

    }

  }



  // Funcion para ir a la pagina anterior
  const paginaAnterior = () => {
    const nuevaPaginaActual = paginaactual - 1
    if (nuevaPaginaActual === 0) return;

    guardarPaginaActual(nuevaPaginaActual)
  }

  // Funcion para ir a la pagina siguiente
  const paginaSiguiente = () => {
    const nuevaPaginaActual = paginaactual + 1
    if (nuevaPaginaActual > paginastotales) return;

    guardarPaginaActual(nuevaPaginaActual)
  }

  return (
    <div className="container">
      <div className="jumbotron">
        <p className="lead text-center"> Buscador de Imagenes</p>
        <Formulario
          guardarTermino={guardarTermino}
          guardarPaginaActual={guardarPaginaActual}
        />
      </div>
      <div className="row justify-content-center">
        <ListadoImagenes
          imagenes={imagenes}
        />


        {termino ?
          <div className="">
            <ul className="pagination flex-wrap">
              {(paginaactual === 1) ? (
                <li className="page-item disabled">
                  <a className="page-link" href="#!">&laquo; Anterior</a>
                </li>
              ) : (
                  <li className="page-item">
                    <a className="page-link" href="#!" onClick={paginaAnterior}>&laquo; Anterior</a>
                  </li>
                )}
              {numpaginas.map(numero => (
                // <a href="#!" onClick={() => guardarPaginaActual(numero)}>{numero}</a>
                <li className="page-item" key={numero} id={numero}>
                  <a className="page-link" href="#!" onClick={() => guardarPaginaActual(numero)}>{numero}</a>
                </li>
              ))}

              {(paginaactual >= paginastotales) ? (
                <li className="page-item disabled">
                  <a className="page-link" href="#!">Siguiente &raquo;</a>
                </li>
              ) : (
                  <li className="page-item">
                    <a className="page-link" href="#!" onClick={paginaSiguiente}>Siguiente &raquo;</a>
                  </li>

                )}
            </ul>
          </div>
          : null}


        {/*         
        <nav aria-label="Page navigation example">
          <ul className="pagination">
            <li class="page-item"><a class="page-link" href="#">Previous</a></li>
            {numpaginas.map(numero => (
              // <a href="#" onClick={() => guardarPaginaActual(numero)}>{numero}</a>
              <li class="page-item"><a class="page-link" href="#" onClick={() => guardarPaginaActual(numero)}>{numero}</a></li>
            ))}
            <li class="page-item"><a class="page-link" href="#">Next</a></li>
          </ul>
        </nav> */}


        {/* {(paginaactual === 1) ? null : (
          <button
            type="button"
            className="btn btn-primary mr-2"
            onClick={paginaAnterior}
          >&laquo; Anterior</button>

        )}

        {(paginaactual >= paginastotales) ? null : (
          <button
            type="button"
            className="btn btn-primary"
            onClick={paginaSiguiente}
          >Siguiente &raquo;</button>

        )} */}



      </div>
    </div>
  );
}

export default App;
