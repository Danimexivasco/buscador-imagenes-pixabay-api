import React, { useState } from 'react';
import Error from './Error'


const Formulario = ({guardarTermino, guardarPaginaActual}) => {

    // State de palabra buscada
    const [busqueda, guardarBusqueda] = useState('')
    // State de errores
    const [error, guardarError] = useState(false);

    const enviarBusqueda = e =>{
        e.preventDefault()

        // Validacion
        if(busqueda.trim() === ''){
            guardarError(true);
            return;
        }
        guardarError(false)
        // Pasar el state al componente principal
        guardarTermino(busqueda)
        guardarPaginaActual(1)
    }

    return (
        <form
            onSubmit={enviarBusqueda}
        >
            <div className="row">
                <div className="form-group col-md-8">
                    <input
                        type="text"
                        className="form-control form-control-lg"
                        placeholder="Introduce un tema o palabra, por ejemplo: Galaxias o Humanidad"
                        onChange={e =>guardarBusqueda(e.target.value)}
                    />
                </div>
                <div className="form-group col-md-4">
                    <input
                        type="submit"
                        className="btn btn-lg btn-warning btn-block"
                        value="Buscar"
                    />
                </div>
            </div>
            {error? <Error msg="Es necesario que se introduzca algo para buscar..." />: null}
        </form>
    );
}

export default Formulario;